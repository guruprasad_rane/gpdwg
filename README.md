gpdwg
=====

A python package to read and write [.dwg](https://en.wikipedia.org/wiki/.dwg) files.

It follows dwg file specifications from [Open Design Specification for .dwg files version 5.4.1](https://www.opendesign.com/files/guestdownloads/OpenDesign_Specification_for_.dwg_files.pdf) prepared by Open Design Alliance.



Documentation
-------------

[gpdwg documentation can be found here.](https://guruprasad_rane.gitlab.io/gpdwg/)


References
----------
Following open source projects were referred while working on gpdwg.
* [libdwg](http://libdwg.sourceforge.net/en/index.html) developed by Felipe Castro
* [libredwg](http://www.gnu.org/software/libredwg/) 