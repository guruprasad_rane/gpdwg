===============
gpdwg variables
===============
.. module:: gpdwg

.. toctree::
   :maxdepth: 2

Class Explaination
------------------
.. class:: gpdwgData.gpdwgVariables

   DWG header vaiables are stored in *gpdwgVariables* class. Each variable can be accessed or modified using following attributes.


   =====================================  ===========  ====================================================
   Variables                              Type         Version
   =====================================  ===========  ====================================================
   SIZE_IN_BITS                           int          Available in DWG version 2007 only.
   REQUIREDVERSIONS                       int          Available in DWG version 2013 and above only.
   UNKNOWN_01                             float        Available in all DWG versions.
   UNKNOWN_02                             float        Available in all DWG versions.
   UNKNOWN_03                             float        Available in all DWG versions.
   UNKNOWN_04                             float        Available in all DWG versions.
   UNKNOWN_05                             str          Available in all DWG versions.
   UNKNOWN_06                             str          Available in all DWG versions.
   UNKNOWN_07                             str          Available in all DWG versions.
   UNKNOWN_08                             str          Available in all DWG versions.
   UNKNOWN_09                             float        Available in all DWG versions.
   UNKNOWN_10                             float        Available in all DWG versions.
   UNKNOWN_11                             float        Available in DWG version 13 and 14 only.
   CURRENT_VIEWPORT_HANDLE                bit.handle   Available in DWG version 13, 14 and 2000 only.
   DIMASO                                 int          Available in all DWG versions.
   DIMSHO                                 int          Available in all DWG versions.
   DIMSAV                                 int          Available in DWG version 13 and 14 only.
   PLINEGEN                               int          Available in all DWG versions.
   ORTHOMODE                              int          Available in all DWG versions.
   REGENMODE                              int          Available in all DWG versions.
   FILLMODE                               int          Available in all DWG versions.
   QTEXTMODE                              int          Available in all DWG versions.
   PSLTSCALE                              int          Available in all DWG versions.
   LIMCHECK                               int          Available in all DWG versions.
   BLIPMODE                               int          Available in DWG version 13 and 14 only.
   UNDOCUMENTED                           int          Available in DWG version 2004 and above only.
   USRTIMER                               int          Available in all DWG versions.
   SKPOLY                                 int          Available in all DWG versions.
   ANGDIR                                 int          Available in all DWG versions.
   SPLFRAME                               int          Available in all DWG versions.
   ATTREQ                                 int          Available in DWG version 13 and 14 only.
   ATTDIA                                 int          Available in DWG version 13 and 14 only.
   MIRRTEXT                               int          Available in all DWG versions.
   WORLDVIEW                              int          Available in all DWG versions.
   WIREFRAME                              int          Available in DWG version 13 and 14 only.
   TILEMODE                               int          Available in all DWG versions.
   PLIMCHECK                              int          Available in all DWG versions.
   VISRETAIN                              int          Available in all DWG versions.
   DELOBJ                                 int          Available in DWG version 13 and 14 only.
   DISPSILH                               int          Available in all DWG versions.
   PELLIPSE                               int          Available in all DWG versions.
   PROXYGRAPHICS                          float        Available in all DWG versions.
   DRAGMODE                               float        Available in DWG version 13 and 14 only.
   TREEDEPTH                              float        Available in all DWG versions.
   LUNITS                                 float        Available in all DWG versions.
   LUPREC                                 float        Available in all DWG versions.
   AUNITS                                 float        Available in all DWG versions.
   AUPREC                                 float        Available in all DWG versions.
   OSMODE                                 float        Available in DWG version 13 and 14 only.
   ATTMODE                                float        Available in all DWG versions.
   COORDS                                 float        Available in DWG version 13 and 14 only.
   PDMODE                                 float        Available in all DWG versions.
   PICKSTYLE                              float        Available in DWG version 13 and 14 only.
   UNKNOWN_12                             float        Available in DWG version 2004 and above only.
   UNKNOWN_13                             float        Available in DWG version 2004 and above only.
   UNKNOWN_14                             float        Available in DWG version 2004 and above only.
   USERI1                                 float        Available in all DWG versions.
   USERI2                                 float        Available in all DWG versions.
   USERI3                                 float        Available in all DWG versions.
   USERI4                                 float        Available in all DWG versions.
   USERI5                                 float        Available in all DWG versions.
   SPLINESEGS                             float        Available in all DWG versions.
   SURFU                                  float        Available in all DWG versions.
   SURFV                                  float        Available in all DWG versions.
   SURFTYPE                               float        Available in all DWG versions.
   SURFTAB1                               float        Available in all DWG versions.
   SURFTAB2                               float        Available in all DWG versions.
   SPLINETYPE                             float        Available in all DWG versions.
   SHADEDGE                               float        Available in all DWG versions.
   SHADEDIF                               float        Available in all DWG versions.
   UNITMODE                               float        Available in all DWG versions.
   MAXACTVP                               float        Available in all DWG versions.
   ISOLINES                               float        Available in all DWG versions.
   CMLJUST                                float        Available in all DWG versions.
   TEXTQLTY                               float        Available in all DWG versions.
   LTSCALE                                float        Available in all DWG versions.
   TEXTSIZE                               float        Available in all DWG versions.
   TRACEWID                               float        Available in all DWG versions.
   SKETCHINC                              float        Available in all DWG versions.
   FILLETRAD                              float        Available in all DWG versions.
   THICKNESS                              float        Available in all DWG versions.
   ANGBASE                                float        Available in all DWG versions.
   PDSIZE                                 float        Available in all DWG versions.
   PLINEWID                               float        Available in all DWG versions.
   USERR1                                 float        Available in all DWG versions.
   USERR2                                 float        Available in all DWG versions.
   USERR3                                 float        Available in all DWG versions.
   USERR4                                 float        Available in all DWG versions.
   USERR5                                 float        Available in all DWG versions.
   CHAMFERA                               float        Available in all DWG versions.
   CHAMFERB                               float        Available in all DWG versions.
   CHAMFERC                               float        Available in all DWG versions.
   CHAMFERD                               float        Available in all DWG versions.
   FACETRES                               float        Available in all DWG versions.
   CMLSCALE                               float        Available in all DWG versions.
   CELTSCALE                              float        Available in all DWG versions.
   MENUNAME                               str          Available in DWG version 13, 14, 2000 and 2004 only.
   TDCREATE_JD                            float        Available in all DWG versions.
   TDCREATE_MS                            float        Available in all DWG versions.
   TDUPDATE_JD                            float        Available in all DWG versions.
   TDUPDATE_MS                            float        Available in all DWG versions.
   UNKNOWN_15                             float        Available in DWG version 2004 and above only.
   UNKNOWN_16                             float        Available in DWG version 2004 and above only.
   UNKNOWN_17                             float        Available in DWG version 2004 and above only.
   TDINDWG_JD                             float        Available in all DWG versions.
   TDINDWG_MS                             float        Available in all DWG versions.
   TDUSRTIMER_JD                          float        Available in all DWG versions.
   TDUSRTIMER_MS                          float        Available in all DWG versions.
   CECOLOR                                bit.color    Available in all DWG versions.
   HANDSEED                               bit.handle   Available in all DWG versions.
   CLAYER                                 bit.handle   Available in all DWG versions.
   TEXTSTYLE                              bit.handle   Available in all DWG versions.
   CELTYPE                                bit.handle   Available in all DWG versions.
   CMATERIAL                              bit.handle   Available in DWG version 2007 and above only.
   DIMSTYLE                               bit.handle   Available in all DWG versions.
   CMLSTYLE                               bit.handle   Available in all DWG versions.
   PSVPSCALE                              float        Available in DWG version 2000 and above only.
   INSBASE_PS                             bit.point3D  Available in all DWG versions.
   EXTMIN_PS                              bit.point3D  Available in all DWG versions.
   EXTMAX_PS                              bit.point3D  Available in all DWG versions.
   LIMMIN_PS                              bit.point2D  Available in all DWG versions.
   LIMMAX_PS                              bit.point2D  Available in all DWG versions.
   ELEVATION_PS                           float        Available in all DWG versions.
   UCSORG_PS                              bit.point3D  Available in all DWG versions.
   UCSXDIR_PS                             bit.point3D  Available in all DWG versions.
   UCSYDIR_PS                             bit.point3D  Available in all DWG versions.
   UCSNAME_PS                             bit.handle   Available in all DWG versions.
   PUCSORTHOREF                           bit.handle   Available in DWG version 2000 and above only.
   PUCSORTHOVIEW                          float        Available in DWG version 2000 and above only.
   PUCSBASE                               bit.handle   Available in DWG version 2000 and above only.
   PUCSORGTOP                             bit.point3D  Available in DWG version 2000 and above only.
   PUCSORGBOTTOM                          bit.point3D  Available in DWG version 2000 and above only.
   PUCSORGLEFT                            bit.point3D  Available in DWG version 2000 and above only.
   PUCSORGRIGHT                           bit.point3D  Available in DWG version 2000 and above only.
   PUCSORGFRONT                           bit.point3D  Available in DWG version 2000 and above only.
   PUCSORGBACK                            bit.point3D  Available in DWG version 2000 and above only.
   INSBASE_MS                             bit.point3D  Available in all DWG versions.
   EXTMIN_MS                              bit.point3D  Available in all DWG versions.
   EXTMAX_MS                              bit.point3D  Available in all DWG versions.
   LIMMIN_MS                              bit.point2D  Available in all DWG versions.
   LIMMAX_MS                              bit.point2D  Available in all DWG versions.
   ELEVATION_MS                           float        Available in all DWG versions.
   UCSORG_MS                              bit.point3D  Available in all DWG versions.
   UCSXDIR_MS                             bit.point3D  Available in all DWG versions.
   UCSYDIR_MS                             bit.point3D  Available in all DWG versions.
   UCSNAME_MS                             bit.handle   Available in all DWG versions.
   UCSORTHOREF                            bit.handle   Available in DWG version 2000 and above only.
   UCSORTHOVIEW                           float        Available in DWG version 2000 and above only.
   UCSBASE                                bit.handle   Available in DWG version 2000 and above only.
   UCSORGTOP                              bit.point3D  Available in DWG version 2000 and above only.
   UCSORGBOTTOM                           bit.point3D  Available in DWG version 2000 and above only.
   UCSORGLEFT                             bit.point3D  Available in DWG version 2000 and above only.
   UCSORGRIGHT                            bit.point3D  Available in DWG version 2000 and above only.
   UCSORGFRONT                            bit.point3D  Available in DWG version 2000 and above only.
   UCSORGBACK                             bit.point3D  Available in DWG version 2000 and above only.
   DIMPOST                                str          Available in DWG version 2000 and above only.
   DIMAPOST                               str          Available in DWG version 2000 and above only.
   DIMTOL                                 int          Available in DWG version 13 and 14 only.
   DIMLIM                                 int          Available in DWG version 13 and 14 only.
   DIMTIH                                 int          Available in DWG version 13 and 14 only.
   DIMTOH                                 int          Available in DWG version 13 and 14 only.
   DIMSE1                                 int          Available in DWG version 13 and 14 only.
   DIMSE2                                 int          Available in DWG version 13 and 14 only.
   DIMALT                                 int          Available in DWG version 13 and 14 only.
   DIMTOFL                                int          Available in DWG version 13 and 14 only.
   DIMSAH                                 int          Available in DWG version 13 and 14 only.
   DIMTIX                                 int          Available in DWG version 13 and 14 only.
   DIMSOXD                                int          Available in DWG version 13 and 14 only.
   DIMALTD                                int          Available in DWG version 13 and 14 only.
   DIMZIN                                 int          Available in DWG version 13 and 14 only.
   DIMSD1                                 int          Available in DWG version 13 and 14 only.
   DIMSD2                                 int          Available in DWG version 13 and 14 only.
   DIMTOLJ                                int          Available in DWG version 13 and 14 only.
   DIMJUST                                int          Available in DWG version 13 and 14 only.
   DIMFIT                                 int          Available in DWG version 13 and 14 only.
   DIMUPT                                 int          Available in DWG version 13 and 14 only.
   DIMTZIN                                int          Available in DWG version 13 and 14 only.
   DIMALTZ                                int          Available in DWG version 13 and 14 only.
   DIMALTTZ                               int          Available in DWG version 13 and 14 only.
   DIMTAD                                 int          Available in DWG version 13 and 14 only.
   DIMUNIT                                float        Available in DWG version 13 and 14 only.
   DIMAUNIT                               float        Available in DWG version 13 and 14 only.
   DIMDEC                                 float        Available in DWG version 13 and 14 only.
   DIMTDEC                                float        Available in DWG version 13 and 14 only.
   DIMALTU                                float        Available in DWG version 13 and 14 only.
   DIMALTTD                               float        Available in DWG version 13 and 14 only.
   DIMTXSTY                               bit.handle   Available in DWG version 13 and 14 only.
   DIMSCALE                               float        Available in all DWG versions.
   DIMASZ                                 float        Available in all DWG versions.
   DIMEXO                                 float        Available in all DWG versions.
   DIMDLI                                 float        Available in all DWG versions.
   DIMEXE                                 float        Available in all DWG versions.
   DIMRND                                 float        Available in all DWG versions.
   DIMDLE                                 float        Available in all DWG versions.
   DIMTP                                  float        Available in all DWG versions.
   DIMTM                                  float        Available in all DWG versions.
   DIMFXL                                 float        Available in DWG version 2007 and above only.
   DIMJOGANG                              float        Available in DWG version 2007 and above only.
   DIMTFILL                               float        Available in DWG version 2007 and above only.
   DIMTFILLCLR                            bit.color    Available in DWG version 2007 and above only.
   DIMTOL                                 int          Available in DWG version 2000 and above only.
   DIMLIM                                 int          Available in DWG version 2000 and above only.
   DIMTIH                                 int          Available in DWG version 2000 and above only.
   DIMTOH                                 int          Available in DWG version 2000 and above only.
   DIMSE1                                 int          Available in DWG version 2000 and above only.
   DIMSE2                                 int          Available in DWG version 2000 and above only.
   DIMTAD                                 float        Available in DWG version 2000 and above only.
   DIMZIN                                 float        Available in DWG version 2000 and above only.
   DIMAZIN                                float        Available in DWG version 2000 and above only.
   DIMARCSYM                              float        Available in DWG version 2007 and above only.
   DIMTXT                                 float        Available in all DWG versions.
   DIMCEN                                 float        Available in all DWG versions.
   DIMTSZ                                 float        Available in all DWG versions.
   DIMALTF                                float        Available in all DWG versions.
   DIMLFAC                                float        Available in all DWG versions.
   DIMTVP                                 float        Available in all DWG versions.
   DIMTFAC                                float        Available in all DWG versions.
   DIMGAP                                 float        Available in all DWG versions.
   DIMPOST                                str          Available in DWG version 13 and 14 only.
   DIMAPOST                               str          Available in DWG version 13 and 14 only.
   DIMBLK                                 str          Available in DWG version 13 and 14 only.
   DIMBLK1                                str          Available in DWG version 13 and 14 only.
   DIMBLK2                                str          Available in DWG version 13 and 14 only.
   DIMALTRND                              float        Available in DWG version 2000 and above only.
   DIMALT                                 int          Available in DWG version 2000 and above only.
   DIMALTD                                float        Available in DWG version 2000 and above only.
   DIMTOFL                                int          Available in DWG version 2000 and above only.
   DIMSAH                                 int          Available in DWG version 2000 and above only.
   DIMTIX                                 int          Available in DWG version 2000 and above only.
   DIMSOXD                                int          Available in DWG version 2000 and above only.
   DIMCLRD                                bit.color    Available in all DWG versions.
   DIMCLRE                                bit.color    Available in all DWG versions.
   DIMCLRT                                bit.color    Available in all DWG versions.
   DIMADEC                                float        Available in DWG version 2000 and above only.
   DIMDEC                                 float        Available in DWG version 2000 and above only.
   DIMTDEC                                float        Available in DWG version 2000 and above only.
   DIMALTU                                float        Available in DWG version 2000 and above only.
   DIMALTTD                               float        Available in DWG version 2000 and above only.
   DIMAUNIT                               float        Available in DWG version 2000 and above only.
   DIMFRAC                                float        Available in DWG version 2000 and above only.
   DIMLUNIT                               float        Available in DWG version 2000 and above only.
   DIMDSEP                                float        Available in DWG version 2000 and above only.
   DIMTMOVE                               float        Available in DWG version 2000 and above only.
   DIMJUST                                float        Available in DWG version 2000 and above only.
   DIMSD1                                 int          Available in DWG version 2000 and above only.
   DIMSD2                                 int          Available in DWG version 2000 and above only.
   DIMTOLJ                                float        Available in DWG version 2000 and above only.
   DIMTZIN                                float        Available in DWG version 2000 and above only.
   DIMALTZ                                float        Available in DWG version 2000 and above only.
   DIMALTTZ                               float        Available in DWG version 2000 and above only.
   DIMUPT                                 int          Available in DWG version 2000 and above only.
   DIMATFIT                               float        Available in DWG version 2000 and above only.
   DIMFXLON                               int          Available in DWG version 2007 and above only.
   DIMTXTDIRECTION                        int          Available in DWG version 2010 and above only.
   DIMALTMZF                              float        Available in DWG version 2010 and above only.
   DIMALTMZS                              str          Available in DWG version 2010 and above only.
   DIMMZF                                 float        Available in DWG version 2010 and above only.
   DIMMZS                                 str          Available in DWG version 2010 and above only.
   DIMTXSTY                               bit.handle   Available in DWG version 2000 and above only.
   DIMLDRBLK                              bit.handle   Available in DWG version 2000 and above only.
   DIMBLK                                 bit.handle   Available in DWG version 2000 and above only.
   DIMBLK1                                bit.handle   Available in DWG version 2000 and above only.
   DIMBLK2                                bit.handle   Available in DWG version 2000 and above only.
   DIMLTYPE                               bit.handle   Available in DWG version 2007 and above only.
   DIMLTEX1                               bit.handle   Available in DWG version 2007 and above only.
   DIMLTEX2                               bit.handle   Available in DWG version 2007 and above only.
   DIMLWD                                 float        Available in DWG version 2000 and above only.
   DIMLWE                                 float        Available in DWG version 2000 and above only.
   BLOCK_CONTROL_OBJECT                   bit.handle   Available in all DWG versions.
   LAYER_CONTROL_OBJECT                   bit.handle   Available in all DWG versions.
   STYLE_CONTROL_OBJECT                   bit.handle   Available in all DWG versions.
   LINETYPE_CONTROL_OBJECT                bit.handle   Available in all DWG versions.
   VIEW_CONTROL_OBJECT                    bit.handle   Available in all DWG versions.
   UCS_CONTROL_OBJECT                     bit.handle   Available in all DWG versions.
   VPORT_CONTROL_OBJECT                   bit.handle   Available in all DWG versions.
   APPID_CONTROL_OBJECT                   bit.handle   Available in all DWG versions.
   DIMSTYLE_CONTROL_OBJECT                bit.handle   Available in all DWG versions.
   VIEWPORT_ENTITY_HEADER_CONTROL_OBJECT  bit.handle   Available in DWG version 13, 14 and 2000 only.
   DICTIONARY_ACAD_GROUP                  bit.handle   Available in all DWG versions.
   DICTIONARY_ACAD_MLINESTYLE             bit.handle   Available in all DWG versions.
   DICTIONARY_NAMED_OBJECTS               bit.handle   Available in all DWG versions.
   TSTACKALIGN                            float        Available in DWG version 2000 and above only.
   TSTACKSIZE                             float        Available in DWG version 2000 and above only.
   HYPERLINKBASE                          str          Available in DWG version 2000 and above only.
   STYLESHEET                             str          Available in DWG version 2000 and above only.
   DICTIONARY_LAYOUTS                     bit.handle   Available in DWG version 2000 and above only.
   DICTIONARY_PLOTSETTINGS                bit.handle   Available in DWG version 2000 and above only.
   DICTIONARY_PLOTSTYLES                  bit.handle   Available in DWG version 2000 and above only.
   DICTIONARY_MATERIALS                   bit.handle   Available in DWG version 2004 and above only.
   DICTIONARY_COLORS                      bit.handle   Available in DWG version 2004 and above only.
   DICTIONARY_VISUALSTYLE                 bit.handle   Available in DWG version 2007 and above only.
   UNKNOWN_18                             bit.handle   Available in DWG version 2013 and above only.
   FLAGS                                  float        Available in DWG version 2000 and above only.
   INSUNITS                               float        Available in DWG version 2000 and above only.
   CEPSNTYPE                              float        Available in DWG version 2000 and above only.
   CPSNID                                 bit.handle   Available in DWG version 2000 and above only.
   FINGERPRINTGUID                        str          Available in DWG version 2000 and above only.
   VERSIONGUID                            str          Available in DWG version 2000 and above only.
   SORTENTS                               int          Available in DWG version 2004 and above only.
   INDEXCTL                               int          Available in DWG version 2004 and above only.
   HIDETEXT                               int          Available in DWG version 2004 and above only.
   XCLIPFRAME                             int          Available in DWG version 2004 and above only.
   DIMASSOC                               int          Available in DWG version 2004 and above only.
   HALOGAP                                int          Available in DWG version 2004 and above only.
   OBSCUREDCOLOR                          float        Available in DWG version 2004 and above only.
   INTERSECTIONCOLOR                      float        Available in DWG version 2004 and above only.
   OBSCUREDLTYPE                          int          Available in DWG version 2004 and above only.
   INTERSECTIONDISPLAY                    int          Available in DWG version 2004 and above only.
   PROJECTNAME                            str          Available in DWG version 2004 and above only.
   BLOCK_RECORD_PS                        bit.handle   Available in all DWG versions.
   BLOCK_RECORD_MS                        bit.handle   Available in all DWG versions.
   LTYPE_BYLAYER                          bit.handle   Available in all DWG versions.
   LTYPE_BYBLOCK                          bit.handle   Available in all DWG versions.
   LTYPE_CONTINUOUS                       bit.handle   Available in all DWG versions.
   CAMERADISPLAY                          int          Available in DWG version 2007 and above only.
   UNKNOWN_19                             float        Available in DWG version 2007 and above only.
   UNKNOWN_20                             float        Available in DWG version 2007 and above only.
   UNKNOWN_21                             float        Available in DWG version 2007 and above only.
   STEPSPERSEC                            float        Available in DWG version 2007 and above only.
   STEPSIZE                               float        Available in DWG version 2007 and above only.
   DWFPREC_3D                             float        Available in DWG version 2007 and above only.
   LENSLENGTH                             float        Available in DWG version 2007 and above only.
   CAMERAHEIGHT                           float        Available in DWG version 2007 and above only.
   SOLIDHIST                              int          Available in DWG version 2007 and above only.
   SHOWHIST                               int          Available in DWG version 2007 and above only.
   PSOLWIDTH                              float        Available in DWG version 2007 and above only.
   PSOLHEIGHT                             float        Available in DWG version 2007 and above only.
   LOFTANG1                               float        Available in DWG version 2007 and above only.
   LOFTANG2                               float        Available in DWG version 2007 and above only.
   LOFTMAG1                               float        Available in DWG version 2007 and above only.
   LOFTMAG2                               float        Available in DWG version 2007 and above only.
   LOFTPARAM                              float        Available in DWG version 2007 and above only.
   LOFTNORMALS                            int          Available in DWG version 2007 and above only.
   LATITUDE                               float        Available in DWG version 2007 and above only.
   LONGITUDE                              float        Available in DWG version 2007 and above only.
   NORTHDIRECTION                         float        Available in DWG version 2007 and above only.
   TIMEZONE                               float        Available in DWG version 2007 and above only.
   LIGHTGLYPHDISPLAY                      int          Available in DWG version 2007 and above only.
   TILEMODELIGHTSYNCH                     int          Available in DWG version 2007 and above only.
   DWFFRAME                               int          Available in DWG version 2007 and above only.
   DGNFRAME                               int          Available in DWG version 2007 and above only.
   UNKNOWN_22                             int          Available in DWG version 2007 and above only.
   INTERFERECOLOR                         bit.color    Available in DWG version 2007 and above only.
   INTERFEREOBJVS                         bit.handle   Available in DWG version 2007 and above only.
   INTERFEREVPVS                          bit.handle   Available in DWG version 2007 and above only.
   DRAGVS                                 bit.handle   Available in DWG version 2007 and above only.
   CSHADOW                                int          Available in DWG version 2007 and above only.
   UNKNOWN_23                             float        Available in DWG version 2007 and above only.
   UNKNOWN_24                             float        Available in DWG version 14 and above only.
   UNKNOWN_25                             float        Available in DWG version 14 and above only.
   UNKNOWN_26                             float        Available in DWG version 14 and above only.
   UNKNOWN_27                             float        Available in DWG version 14 and above only.
   CELWEIGHT                              int          Available in all DWG versions.
   ENDCAPS                                int          Available in all DWG versions.
   JOINSTYLE                              int          Available in all DWG versions.
   LWDISPLAY                              int          Available in all DWG versions.
   XEDIT                                  int          Available in all DWG versions.
   EXTNAMES                               int          Available in all DWG versions.
   PSTYLEMODE                             int          Available in all DWG versions.
   OLESTARTUP                             int          Available in all DWG versions.
   =====================================  ===========  ====================================================

   .. note::
      UNKNOWN variables are those which are yet to be reverse engineered.


Example
-------

Code example to output all variables.

Code::

   import gpdwg
   DwgFileData=gpdwg.read("path/to/file.dwg")
   print(str(DwgFileData.variable))


Output::

   Variables
      ANGBASE : 0.0
      ANGDIR : 0
      APPID_CONTROL_OBJECT : Handle(3,1,9)
      ATTMODE : 1.0
      AUNITS : 0.0
      AUPREC : 0.0
      BLOCK_CONTROL_OBJECT : Handle(3,1,1)
      BLOCK_RECORD_MS : Handle(5,1,31)
      BLOCK_RECORD_PS : Handle(5,1,88)
      CECOLOR : Color(256,,,0,0)
      CELTSCALE : 1.0
      CELTYPE : Handle(5,1,21)
      CELWEIGHT : 29
      CEPSNTYPE : 0.0
      CHAMFERA : 0.0
      CHAMFERB : 0.0
      CHAMFERC : 0.0
      CHAMFERD : 0.0
      CLAYER : Handle(5,1,16)
      CMLJUST : 0.0
      CMLSCALE : 1.0
      CMLSTYLE : Handle(5,1,24)
      CURRENT_VIEWPORT_HANDLE : Handle(5,0,0)
      DICTIONARY_ACAD_GROUP : Handle(5,1,13)
      DICTIONARY_ACAD_MLINESTYLE : Handle(5,1,23)
      DICTIONARY_LAYOUTS : Handle(5,1,26)
      DICTIONARY_NAMED_OBJECTS : Handle(3,1,12)
      DICTIONARY_PLOTSETTINGS : Handle(5,1,25)
      DICTIONARY_PLOTSTYLES : Handle(5,1,14)
      DIMADEC : 0.0
      DIMALT : 0
      DIMALTD : 2.0
      DIMALTF : 25.4
      DIMALTRND : 0.0
      DIMALTTD : 2.0
      DIMALTTZ : 0.0
      DIMALTU : 2.0
      DIMALTZ : 0.0
      DIMAPOST : ''
      DIMASO : 1
      DIMASZ : 0.18
      DIMATFIT : 3.0
      DIMAUNIT : 0.0
      DIMAZIN : 0.0
      DIMBLK : Handle(5,0,0)
      DIMBLK1 : Handle(5,0,0)
      DIMBLK2 : Handle(5,0,0)
      DIMCEN : 0.09
      DIMCLRD : Color(0,,,0,0)
      DIMCLRE : Color(0,,,0,0)
      DIMCLRT : Color(0,,,0,0)
      DIMDEC : 4.0
      DIMDLE : 0.0
      DIMDLI : 0.38
      DIMDSEP : 46.0
      DIMEXE : 0.18
      DIMEXO : 0.0625
      DIMFRAC : 0.0
      DIMGAP : 0.09
      DIMJUST : 0.0
      DIMLDRBLK : Handle(5,0,0)
      DIMLFAC : 1.0
      DIMLIM : 0
      DIMLUNIT : 2.0
      DIMLWD : 65534.0
      DIMLWE : 65534.0
      DIMPOST : ''
      DIMRND : 0.0
      DIMSAH : 0
      DIMSCALE : 1.0
      DIMSD1 : 0
      DIMSD2 : 0
      DIMSE1 : 0
      DIMSE2 : 0
      DIMSHO : 1
      DIMSOXD : 0
      DIMSTYLE : Handle(5,1,39)
      DIMSTYLE_CONTROL_OBJECT : Handle(3,1,10)
      DIMTAD : 0.0
      DIMTDEC : 4.0
      DIMTFAC : 1.0
      DIMTIH : 1
      DIMTIX : 0
      DIMTM : 0.0
      DIMTMOVE : 0.0
      DIMTOFL : 0
      DIMTOH : 1
      DIMTOL : 0
      DIMTOLJ : 1.0
      DIMTP : 0.0
      DIMTSZ : 0.0
      DIMTVP : 0.0
      DIMTXSTY : Handle(5,1,17)
      DIMTXT : 0.18
      DIMTZIN : 0.0
      DIMUPT : 0
      DIMZIN : 0.0
      DISPSILH : 0
      ELEVATION_MS : 0.0
      ELEVATION_PS : 0.0
      ENDCAPS : 0
      EXTMAX_MS : 3DPoint(-100000000000000000000.000000,-100000000000000000000.000000,-100000000000000000000.000000)
      EXTMAX_PS : 3DPoint(0.000000,0.000000,0.000000)
      EXTMIN_MS : 3DPoint(100000000000000000000.000000,100000000000000000000.000000,100000000000000000000.000000)
      EXTMIN_PS : 3DPoint(0.000000,0.000000,0.000000)
      EXTNAMES : 1
      FACETRES : 0.5
      FILLETRAD : 0.0
      FILLMODE : 1
      FINGERPRINTGUID : '{EB61018A-2D64-4AE5-8687-4CE9D7D70539}'
      FLAGS : 10781.0
      HANDSEED : Handle(0,2,364)
      HYPERLINKBASE : ''
      INSBASE_MS : 3DPoint(0.000000,0.000000,0.000000)
      INSBASE_PS : 3DPoint(0.000000,0.000000,0.000000)
      INSUNITS : 1.0
      ISOLINES : 4.0
      JOINSTYLE : 0
      LAYER_CONTROL_OBJECT : Handle(3,1,2)
      LIMCHECK : 0
      LIMMAX_MS : 2DPoint(12,9)
      LIMMAX_PS : 2DPoint(12,9)
      LIMMIN_MS : 2DPoint(0,0)
      LIMMIN_PS : 2DPoint(0,0)
      LINETYPE_CONTROL_OBJECT : Handle(3,1,5)
      LTSCALE : 1.0
      LTYPE_BYBLOCK : Handle(5,1,20)
      LTYPE_BYLAYER : Handle(5,1,21)
      LTYPE_CONTINUOUS : Handle(5,1,22)
      LUNITS : 2.0
      LUPREC : 4.0
      LWDISPLAY : 0
      MAXACTVP : 64.0
      MENUNAME : '.'
      MIRRTEXT : 0
      OLESTARTUP : 0
      ORTHOMODE : 0
      PDMODE : 0.0
      PDSIZE : 0.0
      PELLIPSE : 0
      PLIMCHECK : 0
      PLINEGEN : 0
      PLINEWID : 0.0
      PROXYGRAPHICS : 1.0
      PSLTSCALE : 1
      PSTYLEMODE : 1
      PSVPSCALE : 0.0
      PUCSBASE : Handle(5,0,0)
      PUCSORGBACK : 3DPoint(0.000000,0.000000,0.000000)
      PUCSORGBOTTOM : 3DPoint(0.000000,0.000000,0.000000)
      PUCSORGFRONT : 3DPoint(0.000000,0.000000,0.000000)
      PUCSORGLEFT : 3DPoint(0.000000,0.000000,0.000000)
      PUCSORGRIGHT : 3DPoint(0.000000,0.000000,0.000000)
      PUCSORGTOP : 3DPoint(0.000000,0.000000,0.000000)
      PUCSORTHOREF : Handle(5,0,0)
      PUCSORTHOVIEW : 0.0
      QTEXTMODE : 0
      REGENMODE : 1
      SHADEDGE : 3.0
      SHADEDIF : 70.0
      SKETCHINC : 0.1
      SKPOLY : 0
      SPLFRAME : 0
      SPLINESEGS : 8.0
      SPLINETYPE : 6.0
      STYLESHEET : ''
      STYLE_CONTROL_OBJECT : Handle(3,1,3)
      SURFTAB1 : 6.0
      SURFTAB2 : 6.0
      SURFTYPE : 6.0
      SURFU : 6.0
      SURFV : 6.0
      TDCREATE_JD : 2458413.0
      TDCREATE_MS : 63066937.0
      TDINDWG_JD : 0.0
      TDINDWG_MS : 34149.0
      TDUPDATE_JD : 2458413.0
      TDUPDATE_MS : 63920033.0
      TDUSRTIMER_JD : 2458413.0
      TDUSRTIMER_MS : 63905386.0
      TEXTQLTY : 50.0
      TEXTSIZE : 0.2
      TEXTSTYLE : Handle(5,1,17)
      THICKNESS : 0.0
      TILEMODE : 1
      TRACEWID : 0.05
      TREEDEPTH : 3020.0
      TSTACKALIGN : 1.0
      TSTACKSIZE : 70.0
      UCSBASE : Handle(5,0,0)
      UCSNAME_MS : Handle(5,0,0)
      UCSNAME_PS : Handle(5,0,0)
      UCSORGBACK : 3DPoint(0.000000,0.000000,0.000000)
      UCSORGBOTTOM : 3DPoint(0.000000,0.000000,0.000000)
      UCSORGFRONT : 3DPoint(0.000000,0.000000,0.000000)
      UCSORGLEFT : 3DPoint(0.000000,0.000000,0.000000)
      UCSORGRIGHT : 3DPoint(0.000000,0.000000,0.000000)
      UCSORGTOP : 3DPoint(0.000000,0.000000,0.000000)
      UCSORG_MS : 3DPoint(0.000000,0.000000,0.000000)
      UCSORG_PS : 3DPoint(0.000000,0.000000,0.000000)
      UCSORTHOREF : Handle(5,0,0)
      UCSORTHOVIEW : 0.0
      UCSXDIR_MS : 3DPoint(1.000000,0.000000,0.000000)
      UCSXDIR_PS : 3DPoint(1.000000,0.000000,0.000000)
      UCSYDIR_MS : 3DPoint(0.000000,1.000000,0.000000)
      UCSYDIR_PS : 3DPoint(0.000000,1.000000,0.000000)
      UCS_CONTROL_OBJECT : Handle(3,1,7)
      UNITMODE : 0.0
      UNKNOWN_01 : 412148564080.0
      UNKNOWN_02 : 1.0
      UNKNOWN_03 : 1.0
      UNKNOWN_04 : 1.0
      UNKNOWN_05 : 'm'
      UNKNOWN_06 : ''
      UNKNOWN_07 : ''
      UNKNOWN_08 : ''
      UNKNOWN_09 : 0.0
      UNKNOWN_10 : 0.0
      UNKNOWN_24 : 65535.0
      UNKNOWN_25 : 65535.0
      UNKNOWN_26 : 65535.0
      UNKNOWN_27 : 65535.0
      USERI1 : 0.0
      USERI2 : 0.0
      USERI3 : 0.0
      USERI4 : 0.0
      USERI5 : 0.0
      USERR1 : 0.0
      USERR2 : 0.0
      USERR3 : 0.0
      USERR4 : 0.0
      USERR5 : 0.0
      USRTIMER : 1
      VERSIONGUID : '{7D0B7842-5B0D-4614-BF2D-B838D932E26D}'
      VIEWPORT_ENTITY_HEADER_CONTROL_OBJECT : Handle(3,2,327)
      VIEW_CONTROL_OBJECT : Handle(3,1,6)
      VISRETAIN : 1
      VPORT_CONTROL_OBJECT : Handle(3,1,8)
      WORLDVIEW : 1
      XEDIT : 1
