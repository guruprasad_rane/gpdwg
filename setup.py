import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gpdwg",
    version="0.3.1",
    author='Guruprasad Rane',
    author_email='raneguruprasad@gmail.com',
    description='Package to read and write .dwg files.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/guruprasad_rane/gpdwg',
    project_urls={
        "Bug Tracker": "https://gitlab.com/guruprasad_rane/gpdwg/issues",
        "Documentation": "https://guruprasad_rane.gitlab.io/gpdwg/",
        "Source Code": "https://gitlab.com/guruprasad_rane/gpdwg/-/archive/master/gpdwg-master.tar.gz",
    },
    packages=setuptools.find_packages(exclude=["artwork", "autotest", "bin", "code generator","dist","doc"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
)
