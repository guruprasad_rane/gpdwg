#############################################################################
#  gpdwg - free python package to read .dwg files.                          #
#                                                                           #
#  Copyright (C) 2018 Guruprasad Rane                                       #
#                                                                           #
#  This library is free software, licensed under the terms of the GNU       #
#  General Public License version 2. You should have received a copy of     #
#  the GNU General Public License along with this program.                  #
#                                                                           #
#############################################################################
import logging, mmap

logger = logging.getLogger("gpdwg_logger")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler('gpdwg.log')
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

import os.path
import bit

Versions={'AC1012':13, 'AC1014':14, 'AC1015':2000, 'AC1018':2004, 'AC1021':2007, 'AC1024':2010, 'AC1027':2013}





class gpdwgVariables(object):
	"""Class for Variables"""
	__slots__=['CELWEIGHT','ENDCAPS','JOINSTYLE','LWDISPLAY','XEDIT','EXTNAMES','PSTYLEMODE','OLESTARTUP','SIZE_IN_BITS','REQUIREDVERSIONS','UNKNOWN_01','UNKNOWN_02','UNKNOWN_03','UNKNOWN_04','UNKNOWN_05','UNKNOWN_06','UNKNOWN_07','UNKNOWN_08','UNKNOWN_09','UNKNOWN_10','UNKNOWN_11','CURRENT_VIEWPORT_HANDLE','DIMASO','DIMSHO','DIMSAV','PLINEGEN','ORTHOMODE','REGENMODE','FILLMODE','QTEXTMODE','PSLTSCALE','LIMCHECK','BLIPMODE','UNDOCUMENTED','USRTIMER','SKPOLY','ANGDIR','SPLFRAME','ATTREQ','ATTDIA','MIRRTEXT','WORLDVIEW','WIREFRAME','TILEMODE','PLIMCHECK','VISRETAIN','DELOBJ','DISPSILH','PELLIPSE','PROXYGRAPHICS','DRAGMODE','TREEDEPTH','LUNITS','LUPREC','AUNITS','AUPREC','OSMODE','ATTMODE','COORDS','PDMODE','PICKSTYLE','UNKNOWN_12','UNKNOWN_13','UNKNOWN_14','USERI1','USERI2','USERI3','USERI4','USERI5','SPLINESEGS','SURFU','SURFV','SURFTYPE','SURFTAB1','SURFTAB2','SPLINETYPE','SHADEDGE','SHADEDIF','UNITMODE','MAXACTVP','ISOLINES','CMLJUST','TEXTQLTY','LTSCALE','TEXTSIZE','TRACEWID','SKETCHINC','FILLETRAD','THICKNESS','ANGBASE','PDSIZE','PLINEWID','USERR1','USERR2','USERR3','USERR4','USERR5','CHAMFERA','CHAMFERB','CHAMFERC','CHAMFERD','FACETRES','CMLSCALE','CELTSCALE','MENUNAME','TDCREATE_JD','TDCREATE_MS','TDUPDATE_JD','TDUPDATE_MS','UNKNOWN_15','UNKNOWN_16','UNKNOWN_17','TDINDWG_JD','TDINDWG_MS','TDUSRTIMER_JD','TDUSRTIMER_MS','CECOLOR','HANDSEED','CLAYER','TEXTSTYLE','CELTYPE','CMATERIAL','DIMSTYLE','CMLSTYLE','PSVPSCALE','INSBASE_PS','EXTMIN_PS','EXTMAX_PS','LIMMIN_PS','LIMMAX_PS','ELEVATION_PS','UCSORG_PS','UCSXDIR_PS','UCSYDIR_PS','UCSNAME_PS','PUCSORTHOREF','PUCSORTHOVIEW','PUCSBASE','PUCSORGTOP','PUCSORGBOTTOM','PUCSORGLEFT','PUCSORGRIGHT','PUCSORGFRONT','PUCSORGBACK','INSBASE_MS','EXTMIN_MS','EXTMAX_MS','LIMMIN_MS','LIMMAX_MS','ELEVATION_MS','UCSORG_MS','UCSXDIR_MS','UCSYDIR_MS','UCSNAME_MS','UCSORTHOREF','UCSORTHOVIEW','UCSBASE','UCSORGTOP','UCSORGBOTTOM','UCSORGLEFT','UCSORGRIGHT','UCSORGFRONT','UCSORGBACK','DIMPOST','DIMAPOST','DIMTOL','DIMLIM','DIMTIH','DIMTOH','DIMSE1','DIMSE2','DIMALT','DIMTOFL','DIMSAH','DIMTIX','DIMSOXD','DIMALTD','DIMZIN','DIMSD1','DIMSD2','DIMTOLJ','DIMJUST','DIMFIT','DIMUPT','DIMTZIN','DIMALTZ','DIMALTTZ','DIMTAD','DIMUNIT','DIMAUNIT','DIMDEC','DIMTDEC','DIMALTU','DIMALTTD','DIMTXSTY','DIMSCALE','DIMASZ','DIMEXO','DIMDLI','DIMEXE','DIMRND','DIMDLE','DIMTP','DIMTM','DIMFXL','DIMJOGANG','DIMTFILL','DIMTFILLCLR','DIMAZIN','DIMARCSYM','DIMTXT','DIMCEN','DIMTSZ','DIMALTF','DIMLFAC','DIMTVP','DIMTFAC','DIMGAP','DIMBLK','DIMBLK1','DIMBLK2','DIMALTRND','DIMCLRD','DIMCLRE','DIMCLRT','DIMADEC','DIMFRAC','DIMLUNIT','DIMDSEP','DIMTMOVE','DIMATFIT','DIMFXLON','DIMTXTDIRECTION','DIMALTMZF','DIMALTMZS','DIMMZF','DIMMZS','DIMLDRBLK','DIMLTYPE','DIMLTEX1','DIMLTEX2','DIMLWD','DIMLWE','BLOCK_CONTROL_OBJECT','LAYER_CONTROL_OBJECT','STYLE_CONTROL_OBJECT','LINETYPE_CONTROL_OBJECT','VIEW_CONTROL_OBJECT','UCS_CONTROL_OBJECT','VPORT_CONTROL_OBJECT','APPID_CONTROL_OBJECT','DIMSTYLE_CONTROL_OBJECT','VIEWPORT_ENTITY_HEADER_CONTROL_OBJECT','DICTIONARY_ACAD_GROUP','DICTIONARY_ACAD_MLINESTYLE','DICTIONARY_NAMED_OBJECTS','TSTACKALIGN','TSTACKSIZE','HYPERLINKBASE','STYLESHEET','DICTIONARY_LAYOUTS','DICTIONARY_PLOTSETTINGS','DICTIONARY_PLOTSTYLES','DICTIONARY_MATERIALS','DICTIONARY_COLORS','DICTIONARY_VISUALSTYLE','UNKNOWN_18','FLAGS','INSUNITS','CEPSNTYPE','CPSNID','FINGERPRINTGUID','VERSIONGUID','SORTENTS','INDEXCTL','HIDETEXT','XCLIPFRAME','DIMASSOC','HALOGAP','OBSCUREDCOLOR','INTERSECTIONCOLOR','OBSCUREDLTYPE','INTERSECTIONDISPLAY','PROJECTNAME','BLOCK_RECORD_PS','BLOCK_RECORD_MS','LTYPE_BYLAYER','LTYPE_BYBLOCK','LTYPE_CONTINUOUS','CAMERADISPLAY','UNKNOWN_19','UNKNOWN_20','UNKNOWN_21','STEPSPERSEC','STEPSIZE','DWFPREC_3D','LENSLENGTH','CAMERAHEIGHT','SOLIDHIST','SHOWHIST','PSOLWIDTH','PSOLHEIGHT','LOFTANG1','LOFTANG2','LOFTMAG1','LOFTMAG2','LOFTPARAM','LOFTNORMALS','LATITUDE','LONGITUDE','NORTHDIRECTION','TIMEZONE','LIGHTGLYPHDISPLAY','TILEMODELIGHTSYNCH','DWFFRAME','DGNFRAME','UNKNOWN_22','INTERFERECOLOR','INTERFEREOBJVS','INTERFEREVPVS','DRAGVS','CSHADOW','UNKNOWN_23','UNKNOWN_24','UNKNOWN_25','UNKNOWN_26','UNKNOWN_27']
	def __setattr__(self,name,value):
		A_Types={'ANGBASE':float,'ANGDIR':int,'APPID_CONTROL_OBJECT':bit.handle,'ATTDIA':int,'ATTMODE':float,'ATTREQ':int,'AUNITS':float,'AUPREC':float,'BLIPMODE':int,'BLOCK_CONTROL_OBJECT':bit.handle,'BLOCK_RECORD_MS':bit.handle,'BLOCK_RECORD_PS':bit.handle,'CAMERADISPLAY':int,'CAMERAHEIGHT':float,'CECOLOR':bit.color,'CELTSCALE':float,'CELTYPE':bit.handle,'CELWEIGHT':int,'CEPSNTYPE':float,'CHAMFERA':float,'CHAMFERB':float,'CHAMFERC':float,'CHAMFERD':float,'CLAYER':bit.handle,'CMATERIAL':bit.handle,'CMLJUST':float,'CMLSCALE':float,'CMLSTYLE':bit.handle,'COORDS':float,'CPSNID':bit.handle,'CSHADOW':int,'CURRENT_VIEWPORT_HANDLE':bit.handle,'DELOBJ':int,'DGNFRAME':int,'DICTIONARY_ACAD_GROUP':bit.handle,'DICTIONARY_ACAD_MLINESTYLE':bit.handle,'DICTIONARY_COLORS':bit.handle,'DICTIONARY_LAYOUTS':bit.handle,'DICTIONARY_MATERIALS':bit.handle,'DICTIONARY_NAMED_OBJECTS':bit.handle,'DICTIONARY_PLOTSETTINGS':bit.handle,'DICTIONARY_PLOTSTYLES':bit.handle,'DICTIONARY_VISUALSTYLE':bit.handle,'DIMADEC':float,'DIMALT':int,'DIMALTD':float,'DIMALTF':float,'DIMALTMZF':float,'DIMALTMZS':str,'DIMALTRND':float,'DIMALTTD':float,'DIMALTTZ':float,'DIMALTU':float,'DIMALTZ':float,'DIMAPOST':str,'DIMARCSYM':float,'DIMASO':int,'DIMASSOC':int,'DIMASZ':float,'DIMATFIT':float,'DIMAUNIT':float,'DIMAZIN':float,'DIMBLK':bit.handle,'DIMBLK1':bit.handle,'DIMBLK2':bit.handle,'DIMCEN':float,'DIMCLRD':bit.color,'DIMCLRE':bit.color,'DIMCLRT':bit.color,'DIMDEC':float,'DIMDLE':float,'DIMDLI':float,'DIMDSEP':float,'DIMEXE':float,'DIMEXO':float,'DIMFIT':int,'DIMFRAC':float,'DIMFXL':float,'DIMFXLON':int,'DIMGAP':float,'DIMJOGANG':float,'DIMJUST':float,'DIMLDRBLK':bit.handle,'DIMLFAC':float,'DIMLIM':int,'DIMLTEX1':bit.handle,'DIMLTEX2':bit.handle,'DIMLTYPE':bit.handle,'DIMLUNIT':float,'DIMLWD':float,'DIMLWE':float,'DIMMZF':float,'DIMMZS':str,'DIMPOST':str,'DIMRND':float,'DIMSAH':int,'DIMSAV':int,'DIMSCALE':float,'DIMSD1':int,'DIMSD2':int,'DIMSE1':int,'DIMSE2':int,'DIMSHO':int,'DIMSOXD':int,'DIMSTYLE':bit.handle,'DIMSTYLE_CONTROL_OBJECT':bit.handle,'DIMTAD':float,'DIMTDEC':float,'DIMTFAC':float,'DIMTFILL':float,'DIMTFILLCLR':bit.color,'DIMTIH':int,'DIMTIX':int,'DIMTM':float,'DIMTMOVE':float,'DIMTOFL':int,'DIMTOH':int,'DIMTOL':int,'DIMTOLJ':float,'DIMTP':float,'DIMTSZ':float,'DIMTVP':float,'DIMTXSTY':bit.handle,'DIMTXT':float,'DIMTXTDIRECTION':int,'DIMTZIN':float,'DIMUNIT':float,'DIMUPT':int,'DIMZIN':float,'DISPSILH':int,'DRAGMODE':float,'DRAGVS':bit.handle,'DWFFRAME':int,'DWFPREC_3D':float,'ELEVATION_MS':float,'ELEVATION_PS':float,'ENDCAPS':int,'EXTMAX_MS':bit.point3D,'EXTMAX_PS':bit.point3D,'EXTMIN_MS':bit.point3D,'EXTMIN_PS':bit.point3D,'EXTNAMES':int,'FACETRES':float,'FILLETRAD':float,'FILLMODE':int,'FINGERPRINTGUID':str,'FLAGS':float,'HALOGAP':int,'HANDSEED':bit.handle,'HIDETEXT':int,'HYPERLINKBASE':str,'INDEXCTL':int,'INSBASE_MS':bit.point3D,'INSBASE_PS':bit.point3D,'INSUNITS':float,'INTERFERECOLOR':bit.color,'INTERFEREOBJVS':bit.handle,'INTERFEREVPVS':bit.handle,'INTERSECTIONCOLOR':float,'INTERSECTIONDISPLAY':int,'ISOLINES':float,'JOINSTYLE':int,'LATITUDE':float,'LAYER_CONTROL_OBJECT':bit.handle,'LENSLENGTH':float,'LIGHTGLYPHDISPLAY':int,'LIMCHECK':int,'LIMMAX_MS':bit.point2D,'LIMMAX_PS':bit.point2D,'LIMMIN_MS':bit.point2D,'LIMMIN_PS':bit.point2D,'LINETYPE_CONTROL_OBJECT':bit.handle,'LOFTANG1':float,'LOFTANG2':float,'LOFTMAG1':float,'LOFTMAG2':float,'LOFTNORMALS':int,'LOFTPARAM':float,'LONGITUDE':float,'LTSCALE':float,'LTYPE_BYBLOCK':bit.handle,'LTYPE_BYLAYER':bit.handle,'LTYPE_CONTINUOUS':bit.handle,'LUNITS':float,'LUPREC':float,'LWDISPLAY':int,'MAXACTVP':float,'MENUNAME':str,'MIRRTEXT':int,'NORTHDIRECTION':float,'OBSCUREDCOLOR':float,'OBSCUREDLTYPE':int,'OLESTARTUP':int,'ORTHOMODE':int,'OSMODE':float,'PDMODE':float,'PDSIZE':float,'PELLIPSE':int,'PICKSTYLE':float,'PLIMCHECK':int,'PLINEGEN':int,'PLINEWID':float,'PROJECTNAME':str,'PROXYGRAPHICS':float,'PSLTSCALE':int,'PSOLHEIGHT':float,'PSOLWIDTH':float,'PSTYLEMODE':int,'PSVPSCALE':float,'PUCSBASE':bit.handle,'PUCSORGBACK':bit.point3D,'PUCSORGBOTTOM':bit.point3D,'PUCSORGFRONT':bit.point3D,'PUCSORGLEFT':bit.point3D,'PUCSORGRIGHT':bit.point3D,'PUCSORGTOP':bit.point3D,'PUCSORTHOREF':bit.handle,'PUCSORTHOVIEW':float,'QTEXTMODE':int,'REGENMODE':int,'REQUIREDVERSIONS':int,'SHADEDGE':float,'SHADEDIF':float,'SHOWHIST':int,'SIZE_IN_BITS':int,'SKETCHINC':float,'SKPOLY':int,'SOLIDHIST':int,'SORTENTS':int,'SPLFRAME':int,'SPLINESEGS':float,'SPLINETYPE':float,'STEPSIZE':float,'STEPSPERSEC':float,'STYLESHEET':str,'STYLE_CONTROL_OBJECT':bit.handle,'SURFTAB1':float,'SURFTAB2':float,'SURFTYPE':float,'SURFU':float,'SURFV':float,'TDCREATE_JD':float,'TDCREATE_MS':float,'TDINDWG_JD':float,'TDINDWG_MS':float,'TDUPDATE_JD':float,'TDUPDATE_MS':float,'TDUSRTIMER_JD':float,'TDUSRTIMER_MS':float,'TEXTQLTY':float,'TEXTSIZE':float,'TEXTSTYLE':bit.handle,'THICKNESS':float,'TILEMODE':int,'TILEMODELIGHTSYNCH':int,'TIMEZONE':float,'TRACEWID':float,'TREEDEPTH':float,'TSTACKALIGN':float,'TSTACKSIZE':float,'UCSBASE':bit.handle,'UCSNAME_MS':bit.handle,'UCSNAME_PS':bit.handle,'UCSORGBACK':bit.point3D,'UCSORGBOTTOM':bit.point3D,'UCSORGFRONT':bit.point3D,'UCSORGLEFT':bit.point3D,'UCSORGRIGHT':bit.point3D,'UCSORGTOP':bit.point3D,'UCSORG_MS':bit.point3D,'UCSORG_PS':bit.point3D,'UCSORTHOREF':bit.handle,'UCSORTHOVIEW':float,'UCSXDIR_MS':bit.point3D,'UCSXDIR_PS':bit.point3D,'UCSYDIR_MS':bit.point3D,'UCSYDIR_PS':bit.point3D,'UCS_CONTROL_OBJECT':bit.handle,'UNDOCUMENTED':int,'UNITMODE':float,'UNKNOWN_01':float,'UNKNOWN_02':float,'UNKNOWN_03':float,'UNKNOWN_04':float,'UNKNOWN_05':str,'UNKNOWN_06':str,'UNKNOWN_07':str,'UNKNOWN_08':str,'UNKNOWN_09':float,'UNKNOWN_10':float,'UNKNOWN_11':float,'UNKNOWN_12':float,'UNKNOWN_13':float,'UNKNOWN_14':float,'UNKNOWN_15':float,'UNKNOWN_16':float,'UNKNOWN_17':float,'UNKNOWN_18':bit.handle,'UNKNOWN_19':float,'UNKNOWN_20':float,'UNKNOWN_21':float,'UNKNOWN_22':int,'UNKNOWN_23':float,'UNKNOWN_24':float,'UNKNOWN_25':float,'UNKNOWN_26':float,'UNKNOWN_27':float,'USERI1':float,'USERI2':float,'USERI3':float,'USERI4':float,'USERI5':float,'USERR1':float,'USERR2':float,'USERR3':float,'USERR4':float,'USERR5':float,'USRTIMER':int,'VERSIONGUID':str,'VIEWPORT_ENTITY_HEADER_CONTROL_OBJECT':bit.handle,'VIEW_CONTROL_OBJECT':bit.handle,'VISRETAIN':int,'VPORT_CONTROL_OBJECT':bit.handle,'WIREFRAME':int,'WORLDVIEW':int,'XCLIPFRAME':int,'XEDIT':int}
		if name in A_Types:
			if not isinstance(value, A_Types[name]):
				raise TypeError('%s attribute must be set to an instance of %s but found to be %s' % (name, A_Types[name],type(value)))
		object.__setattr__(self, name, value)
	def __repr__(self):
		TempOutput='Variables\n'
		for i in dir(self):
			if not i.startswith('__') and hasattr(self,i):
				TempOutput=TempOutput+'\t'+str(i)+' : '+repr(getattr(self,i))+'\n'
		return TempOutput
DataTypeList={'B':'int','B':'int','2BD':'bit.point2D','2RD':'bit.point2D','3B':'bit.point3D','3BD':'bit.point3D','3DD':'bit.point3D','3RD':'bit.point3D','BB':'float','BD':'float','BE':'int','BL':'float','BLL':'int','BS':'float','BT':'int','CMC':'bit.color','DD':'float','H':'bit.handle','MC':'int','MS':'int','RC':'int','RD':'float','RL':'int','RS':'int','SN':'int','T':'str','TC':'int','TU':'int','TV':'str','U':'float'}

class bitData(object):
	"""Bitwise Data for dwg file."""
	def __init__(self, chain="",byte=0,size=0,bit=0):
		if not isinstance(byte, int): raise TypeError("For class bitData byte attribute must be set to an integer")
		if not isinstance(size, int): raise TypeError("For class bitData size attribute must be set to an integer")
		if not isinstance(bit, int): raise TypeError("For class bitData bit attribute must be set to an integer")
		self.chain = chain
		self.byte = byte
		self.size = size
		self.bit = bit

class gpdwgData(object):
	"""Data strucutre for DWG output"""
	class gpdwgClass(object):
		"""Class for gpdwg Class strucutre """
		__slots__=['CLASS_NUM','VERSION','APPNAME','CPLUSPLUSCLASSNAME','CLASSDXFNAME','WASAZOMBIE','ITEMCLASSID','PROXY_FLAGS','NUM_OF_OBJECTS','DWG_VERSION','MAINTENANCE_VERSION','UNKNOWN1','UNKNOWN2']
		def __setattr__(self,name,value):
			A_Types={'CLASS_NUM':float,'VERSION':float,'APPNAME':str,'CPLUSPLUSCLASSNAME':str,'CLASSDXFNAME':str,'WASAZOMBIE':int,'ITEMCLASSID':float,'PROXY_FLAGS':float,'NUM_OF_OBJECTS':float,'DWG_VERSION':float,'MAINTENANCE_VERSION':float,'UNKNOWN1':float,'UNKNOWN2':float}
			if name in A_Types:
				if not isinstance(value, A_Types[name]):
					raise TypeError('%s attribute must be set to an instance of %s but found to be %s' % (name, A_Types[name],type(value)))
			object.__setattr__(self, name, value)
		def __repr__(self):
			TempOutput="Classes\n"
			for i in dir(self):
				if not i.startswith('__') and hasattr(self,i):
					TempOutput=TempOutput+"\t"+str(i)+' : '+repr(getattr(self,i))+'\n'
			return TempOutput


	def __init__(self, v=gpdwgVariables(), ver=0, h={'section': {}, 'sectionCount':0},c=[],cc=0,o={},oc=0):
		self.header = h
		self.variable = v
		self.classes = c
		self.classcount = cc
		self.version = ver
		self.objects = o
		self.objectcount=oc
	def __repr__(self):
		TempOutput=""
		TempOutput=TempOutput+"Header\n\n"
		TempOutput=TempOutput+repr(self.header)

		TempOutput=TempOutput+"Objects\n\n"+str(self.objectcount) + " objects found.\n\n"
		for i in xrange(self.objectcount):
			TempOutput=TempOutput+"\n"+str(i)+": " + str(self.objects[i].__name__) + "\n"
			TempOutput=TempOutput+repr(self.objects[i])

		return TempOutput

def read(dwgFilePath):
	if not os.path.isfile(dwgFilePath):
		logger.error( "File %s not found." % dwgFilePath)
		quit()
	if not dwgFilePath.lower().endswith('.dwg'):
		logger.error( "Only files with .dwg extension can be decoded.")
		quit()

	dat=bitData() #Variable that will hold dwg files bitwise data
	gpdwg=gpdwgData() #Variable that will hold data output
	with open(dwgFilePath,"r+b") as f:
		dat.size = os.path.getsize(dwgFilePath)
		#logger.error(str(os.path.getsize(dwgFilePath)))
		dat.chain= mmap.mmap(f.fileno(), 0)

	gpdwg.header['ACADMAINTVER']=dat.chain[0:6].decode("utf-8")
	gpdwg.header['ACADVER']=Versions[gpdwg.header['ACADMAINTVER']]
	dat.version=Versions[gpdwg.header['ACADMAINTVER']]

	if gpdwg.header['ACADMAINTVER']=='AC1015':
		import version
		version.decode2000(gpdwg,dat)
	return gpdwg

def getPreview(dwgFilePath):
	if not os.path.isfile(dwgFilePath):
		quit()
	if not dwgFilePath.lower().endswith('.dwg'):
		logger.error( "Only files with .dwg extension can be decoded.")
		quit()

	dat=bitData() #Variable that will hold dwg files bitwise data
	with open(dwgFilePath,"r+b") as f:
		dat.size = os.path.getsize(dwgFilePath)
		#logger.error(str(os.path.getsize(dwgFilePath)))
		dat.chain= mmap.mmap(f.fileno(), 0)
	if dat.chain[0:6].decode("utf-8") == 'AC1015':
		dat.byte=71
		Section5Start=bit.readwithlog("RL",dat,"Section 5 Address",1)
		Section5Size=bit.readwithlog("RL",dat,"Section 5 Size",1)
		dat.byte=Section5Start+Section5Size
	elif dat.chain[0:6].decode("utf-8") == 'AC1018' or dat.chain[0:6].decode("utf-8") == 'AC1021' or  dat.chain[0:6].decode("utf-8") == 'AC1024' or  dat.chain[0:6].decode("utf-8") == 'AC1027' or  dat.chain[0:6].decode("utf-8") == 'AC1032':
		dat.byte=13
		PreviewStart=bit.readwithlog("RL",dat,"Preview Start",1)
		dat.byte=PreviewStart

	if bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_PICTURE_BEGIN'))):
		Preview_Size=bit.readwithlog('RL',dat,'Preview_Size',1)
		Image_Present=bit.readwithlog('RC',dat,'Image_Present',1)
		from PIL import Image
		for i in range(Image_Present):
			Code=bit.readwithlog('RC',dat,'Code',1)
			if Code == 1 :
				Header_Start = bit.readwithlog('RL',dat,'Header Start',1)
				Header_Size = bit.readwithlog('RL',dat,'Header Size',1)
			elif Code == 2 :
				bmp_Start = bit.readwithlog('RL',dat,'BMP Start',1)
				bmp_Size = bit.readwithlog('RL',dat,'BMP Size',1)
				dat.byte=bmp_Start
				Preview_BMP_Header_Size=bit.readwithlog('RL',dat,'Header Size',1)
				Preview_BMP_Image_Width=bit.readwithlog('RL',dat,'Image Width',1)	
				Preview_BMP_Image_Height=bit.readwithlog('RL',dat,'Image Height',1)	
				Preview_BMP_Color_Plane=bit.readwithlog('RS',dat,'Color Plane',1)	
				Preview_BMP_Bits_Per_Pixel=bit.readwithlog('RS',dat,'Bits per pixel',1)
				Preview_BMP_Compression=bit.readwithlog('RL',dat,'BMP Compression',1)
				Preview_BMP_Image_Size=bit.readwithlog('RL',dat,'BMP Image Size',1)
				Preview_BMP_Res_Hor=bit.readwithlog('RL',dat,'BMP Resolution Horizontal',1)
				Preview_BMP_Res_Ver=bit.readwithlog('RL',dat,'BMP Resolution Vertical',1)
				Preview_BMP_Num_Colors=bit.readwithlog('RL',dat,'BMP Number of colors',1)
				Preview_BMP_Num_Imp_Colors=bit.readwithlog('RL',dat,'BMP Number of important colors',1)
				ColorTable=[]
				for i in range(256):
					R=bit.read_RC(dat)
					G=bit.read_RC(dat)
					B=bit.read_RC(dat)
					bit.read_RC(dat)
					ColorTable.append((R,G,B))
				logger.debug(dat.byte)
				img = Image.new( 'RGB', (Preview_BMP_Image_Width,Preview_BMP_Image_Height))
				PNG_pixels = img.load()
				for i in range(Preview_BMP_Image_Height):
					for j in range(Preview_BMP_Image_Width):
						PNG_pixels[j,i]=ColorTable[bit.read_RC(dat)]
				if bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_PICTURE_END'))):
					return img
			elif Code == 3:
				wmf_Start = bit.readwithlog('RL',dat,'WMF Start',1)
				wmf_Size = bit.readwithlog('RL',dat,'WMF Size',1)
			elif Code == 6:
				PNG_Start = bit.readwithlog('RL',dat,'PNG Start',1)
				PNG_Size = bit.readwithlog('RL',dat,'PNG Size',1)
				dat.byte=PNG_Start
				f = open("demofile.png", "wb")
				import struct, io
				TempBytes=bytearray(PNG_Size)
				for i  in range(PNG_Size):
					struct.pack_into("B",TempBytes,i,bit.read_RC(dat))
				img = Image.open(io.BytesIO(TempBytes))
				if bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_PICTURE_END'))):
					return img
			else:
				logger.error("Unknown Preview Code found.")
