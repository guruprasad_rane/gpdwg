#############################################################################
#  gpdwg - free python package to read .dwg files.                          #
#                                                                           #
#  Copyright (C) 2018 Guruprasad Rane                                       #
#                                                                           #
#  This library is free software, licensed under the terms of the GNU       #
#  General Public License version 2. You should have received a copy of     #
#  the GNU General Public License along with this program.                  #
#                                                                           #
#############################################################################
import os.path, logging
import bit, variables

logger = logging.getLogger("gpdwg_logger")

def decode2000(gpdwg,dat):
	"Decodes version 2000 dwg file."
	#Decode Codepage
	dat.byte=int('0x13',16)
	gpdwg.header['codepage']= bit.readwithlog("RS",dat,"Codepage",1)#  bit.read_RS(dat)

	#Decode section locator records.
	dat.byte=int('0x15',16)
	gpdwg.header['sectionCount']=bit.readwithlog("RL",dat,"Section Count",1)#bit.read_RL(dat)
	for i in range(gpdwg.header['sectionCount']):
		gpdwg.header['section'][i]={}
		gpdwg.header['section'][i]['number']=bit.readwithlog("RC",dat,"Section "+str(i)+" Number",1) #bit.read_RC(dat)
		gpdwg.header['section'][i]['address']=bit.readwithlog("RL",dat,"Section "+str(i)+" Address",1) #(bit.read_RL(dat))
		gpdwg.header['section'][i]['size']=bit.readwithlog("RL",dat,"Section "+str(i)+" Size",1) #bit.read_RL(dat)	
	for x in range(0,gpdwg.header['sectionCount']):
		logger.info("Section No."+str(x)+" Address:"+str(gpdwg.header['section'][x]['address'])+"\t\tSize:"+str(gpdwg.header['section'][x]['address']+gpdwg.header['section'][x]['size']))

	crc1 = bit.read_crc8 ('0xc0c1', dat.chain, dat.byte)
	crc2 = bit.readwithlog("RS",dat,"CRC2",1) #bit.read_RS (dat)
	if (crc1 == crc2):
		logger.info("CRC matched.")
	else:
		logger.error("CRC mismatch CRC1:" + str(crc1) + " CRC2:" + str(crc2))
	bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_HEADER_END')))


	logger.debug("Shifting dat.byte to preview address.")
	dat.byte=gpdwg.header['section'][5]['address']+gpdwg.header['section'][5]['size']
	dat.bit=0

	bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_PICTURE_BEGIN')))
	Preview_Size=bit.readwithlog('RL',dat,'Preview_Size',1)
	Image_Present=bit.readwithlog('RC',dat,'Image_Present',1)
	for i in range(Image_Present):
		Code=bit.readwithlog('RC',dat,'Code',1)
		if Code == 1 :
			Header_Start = bit.readwithlog('RL',dat,'Header Start',1)
			Header_Size = bit.readwithlog('RL',dat,'Header Size',1)
		elif Code == 2 :
			bmp_Start = bit.readwithlog('RL',dat,'BMP Start',1)
			bmp_Size = bit.readwithlog('RL',dat,'BMP Size',1)
		elif Code == 3:
			wmf_Start = bit.readwithlog('RL',dat,'WMF Start',1)
			wmf_Size = bit.readwithlog('RL',dat,'WMF Size',1)
		else:
			logger.error("Unknown Preview Code found.")

	import struct

	fout = open('test.bmp', 'wb')
	logger.info(str(dat.byte)+" writing Image Header.")
	for i in range(Header_Size):
		TempByte=bit.read_RC(dat)
		#fout.write(str(TempByte)+"\n")
		#fout.write(chr(TempByte))
		#fout.write(struct.pack('i', TempByte))
	logger.info(str(dat.byte)+" writing BMP data.")
	Preview_BMP_Header_Size=bit.readwithlog('RL',dat,'Header Size',1)
	Preview_BMP_Image_Width=bit.readwithlog('RL',dat,'Image Width',1)	
	Preview_BMP_Image_Height=bit.readwithlog('RL',dat,'Image Height',1)	
	Preview_BMP_Color_Plane=bit.readwithlog('RS',dat,'Color Plane',1)	
	Preview_BMP_Bits_Per_Pixel=bit.readwithlog('RS',dat,'Bits per pixel',1)
	Preview_BMP_Compression=bit.readwithlog('RL',dat,'BMP Compression',1)
	Preview_BMP_Image_Size=bit.readwithlog('RL',dat,'BMP Image Size',1)
	Preview_BMP_Res_Hor=bit.readwithlog('RL',dat,'BMP Resolution Horizontal',1)
	Preview_BMP_Res_Ver=bit.readwithlog('RL',dat,'BMP Resolution Vertical',1)
	Preview_BMP_Num_Colors=bit.readwithlog('RL',dat,'BMP Number of colors',1)
	Preview_BMP_Num_Imp_Colors=bit.readwithlog('RL',dat,'BMP Number of important colors',1)

	"""
	logger.info(str(dat.byte))
	fout.write(struct.pack("BB", 66,77))
	fout.write(struct.pack("L", bmp_Size+14))
	fout.write(struct.pack("BBBB", 0,0,0,0))
	"""
	ColorTable=[]
	for i in range(256):
		R=bit.read_RC(dat)
		G=bit.read_RC(dat)
		B=bit.read_RC(dat)
		bit.read_RC(dat)
		ColorTable.append((R,G,B))

	from PIL import Image

	img = Image.new( 'RGB', (Preview_BMP_Image_Width,Preview_BMP_Image_Height))
	pixels = img.load() # create the pixel map


	PixelArray=[]
	for i in range(Preview_BMP_Image_Height):
		TempPixelArray=[]
		for j in range(Preview_BMP_Image_Width):
			#Index=bit.readwithlog('RC',dat,'Index',1)
			Index=bit.read_RC(dat)
			TempPixelArray.append(Index)
			pixels[j,i] = (ColorTable[Index])
			print(pixels[j,i])
		PixelArray.append(TempPixelArray)

	logger.info(str(dat.byte)+" end of BMP.")

	img.show()

	fout.close()

	bit.check_Sentinel(dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_PICTURE_END')))



"""
	#Decode Header Variables
	logger.debug("Shifting dat.byte to section 0 address.")
	dat.byte=gpdwg.header['section'][0]['address']
	dat.bit=0

	bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_VARIABLE_BEGIN')))

	SentinelByte=dat.byte
	logger.debug("Start Reading Varialbles "+str(dat.byte)+" "+str(dat.bit))
	size = bit.readwithlog("RL",dat,"Section Size",1) #bit.read_RL(dat)

	variables.readall(gpdwg,dat)
	if dat.byte < SentinelByte + size + 4:
		logger.info(" " + str(dat.byte)+ " " + str(dat.bit)+ " ODA seems to write one extra unknown byte here. Shifting one byte.")
		dat.byte+=1
	crc1 = bit.read_crc8 ('0xc0c1', dat.chain, size + 4  , SentinelByte )	
	crc2 = bit.readwithlog("RS",dat,"CRC2",1) #bit.read_RS (dat)
	if (crc1 == crc2):
		logger.info("CRC matched.")
	else:
		logger.error("CRC mismatch CRC1:" + str(crc1) + " CRC2:" + str(crc2))

	bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_VARIABLE_END')))

	#Decode Classes

	logger.debug("Shifting dat.byte to section 1 address.")
	dat.byte=gpdwg.header['section'][1]['address']
	dat.bit=0

	bit.check_Sentinel (dat, bit.sentinel (bit.DwgSentinel('DWG_SENTINEL_CLASS_BEGIN')))

	logger.debug(str(dat.byte)+" "+str(dat.bit) + "Start Reading Classes ")
	size = bit.read_RL(dat)
	dat.bit=0
	last = dat.byte + size
	classCount=0
	while dat.byte < (last -1 ):
		TempClass=gpdwg.gpdwgClass()
		TempClass.CLASS_NUM=bit.read_BS(dat)
		TempClass.VERSION=bit.read_BS(dat)
		TempClass.APPNAME=bit.read_TV(dat)
		TempClass.CPLUSPLUSCLASSNAME=bit.read_TV(dat)
		TempClass.CLASSDXFNAME=bit.read_TV(dat)
		TempClass.WASAZOMBIE=bit.read_B(dat)
		TempClass.ITEMCLASSID=bit.read_BS(dat)

		classCount = classCount+ 1
		#gpdwg.classes['count']=classCount
		gpdwg.classes.append(TempClass)

	#Check CRC
	dat.byte=gpdwg.header['section'][1]['address']+gpdwg.header['section'][1]['size'] - 18
	dat.bit=0
	ckr=bit.read_RS(dat)
	#print dat.chain

	ckr2=bit.read_crc8('0xc0c1', dat.chain, gpdwg.header['section'][1]['size'] - 34,gpdwg.header['section'][1]['address'] + 16)
	if ckr != ckr2:
		logger.error("Classes CRC failed! CRC:"+str(ckr)+" CRC2:"+str(ckr2))
	dat.byte=dat.byte+16
	pvz=bit.read_RS(dat)
	logger.debug("Ending Reading Classes "+str(dat.byte))
"""


"""
	# Start Scanning Objects
	objectcount=0
	intObjectBegin=gpdwg.header['section'][1]['address']+gpdwg.header['section'][1]['size']
	#intObjectEnd=gpdwg.header['section'][2]['address']+gpdwg.header['section'][2]['size']
	intObjectEnd= intObjectBegin + 10000
	logger.debug("Scan begins at "+str(intObjectBegin)+" and ends at "+str(intObjectEnd))
	dat.bit=0
	dat.byte = intObjectBegin
	while dat.byte < intObjectEnd:
		prvbyte=dat.byte
		if objects.add(gpdwg,dat):
			gpdwg.objects[objectcount]=gpdwg.objects['temp']
			logger.debug("\t\t\t\t\t\t\t\t Object Found")
			#dat.byte=dat.byte+2
			#gpdwg.objects[objectcount]['start-byte']=prvbyte
			#gpdwg.objects[objectcount]['end-byte']=dat.byte
			gpdwg.objectcount += 1
			objectcount += 1
		else:
			logger.debug("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t Object Not Found")
			dat.byte=prvbyte+1
		#dat.byte=prvbyte+1
		dat.bit=0
		
		gpdwg.objects.pop('temp')
		#dat.byte+=1
"""